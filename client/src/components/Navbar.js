import { Link } from 'react-router-dom';
import { AppBar, Toolbar, IconButton, Typography } from '@material-ui/core';
import styles from './navbar.module.css';

import classNames from 'classnames';

const Navbar = (props) => {

  const toggleButton = () => {
    const navbarLinks = document.getElementsByClassName(styles.navbarLinks)[0];
    navbarLinks.classList.toggle('active');
  }

  return (
    <div>
      <nav className={styles.navbar}>
        <AppBar position="static" color="transparent">
          <Toolbar>
            <IconButton edge="start" aria-label="menu" className={styles.title}>
              <Link to="/">WAMessage Api</Link>
              {/* <MenuIcon /> */}
            </IconButton>
            <Link to="/" onClick={toggleButton} className={styles.toggleButton}>
              <span className={classNames(styles.bar)}></span>
              <span className={classNames(styles.bar)}></span>
              <span className={classNames(styles.bar)}></span>
            </Link>
            <div className={styles.navbarLinks}>
              <ul>
                <li>
                  <Typography variant="h6">
                    <Link to="/message">Product</Link>
                  </Typography>
                </li>
                <li>
                  <Typography variant="h6">
                    <Link to="/message">Pricing</Link>
                  </Typography>
                </li>
                <li>
                  <Typography variant="h6">
                    <Link to="/message">Solutions</Link>
                  </Typography>
                </li>
                <li>
                  <Typography variant="h6">
                    <Link to="/message">Customers</Link>
                  </Typography>
                </li>
                <li>
                  <Typography variant="h6">
                    <Link to="/message">Resources</Link>
                  </Typography>
                </li>
                <li>
                  <Typography variant="h6">
                    <Link to="/message">Send Message</Link>
                  </Typography>
                </li>
                <li>
                  <Typography variant="h6">
                    <Link to="/" onClick={props.onLogout}>Logout</Link>
                  </Typography>
                </li>
              </ul>
            </div>
          </Toolbar>
        </AppBar>
      </nav>
    </div>
  )
}

export default Navbar;

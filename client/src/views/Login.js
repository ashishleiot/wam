import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import { Redirect } from 'react-router-dom';

const Login = (props) => {

  return (
    <div>
      {
        props.user 
          ?
        <Redirect to="/dashboard" />
          :
        <div>
          <StyledFirebaseAuth uiConfig={props.uiConfig} firebaseAuth={props.auth} />
        </div>
      }
    </div>
  )
}

export default Login;

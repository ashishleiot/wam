import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import LockIpenIcon from '@material-ui/icons/LockOpen';
import {AppBar, Toolbar} from '@material-ui/core';
import DoneIcon from '@material-ui/icons/Done';

import styles from './views.module.css';

const Home = (props) => {
  return(
    <div>
      {
        props.user
          ?
        <Redirect to="/dashboard" />
          :
          <div>
            <AppBar position="static" color="transparent">
              <Toolbar>
                {/* <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                  <MenuIcon />
                </IconButton> */}            
                <div className={styles.navbar}>
                  <span>
                    <LockIpenIcon style={{fill: 'orange'}}/>
                  </span>
                  <span>
                    <Link to="/login">Login</Link>
                  </span>
                </div>
              </Toolbar>
            </AppBar>
          <div>
            {/* <img /> */}
              <section className={styles.midRow}>
                <div>
                  <h1>
                    <span className={styles.mainHeading}>Subscription Billing & Revenue Operations</span>
                    <br />
                    <span>for Fast-growth B2B SaaS</span>
                  </h1>
                </div>
                <div>
                  <b> How's your evaluation coming along?</b>
                  <br />
                  <span>Our onboarding team is all geared up to help you implement Chargebee. 🚀</span>
                </div>
                <div>
                  <div className={styles.testBtn}>
                    <Button variant="contained" color="primary">
                      Schedule call
                    </Button>
                  </div>
                </div>
              </section>
              <section>
                <div>
                <div className={styles.data}>
                  <div className={styles.dataHeader}>
                    <div>
												No more spreadsheet errors
											</div>
                    </div>
                    <div>
                      <h2>
												Automate Recurring Billing
											</h2>
                    <div className={styles.dataDesc}>
												Scale your SaaS through 480+ recurring billing scenarios that automate who you bill, when, and how. No humans, no spreadsheets, no missed payments!
										</div>
                    </div> 
                    <div>
                        <ul>
                          <li>
                            <DoneIcon />
													    Billing Schedules
												  </li>
                          <li>
                            <DoneIcon />
													  Proration &amp; Invoicing
												  </li>
                          <li>
                            <DoneIcon />
													  Tax management
												  </li>
                          <li>
                            <DoneIcon />
													  Payment Methods
												  </li>
                        </ul>
                        <div>
                          
                        </div>
                      </div>
                    </div>
                </div>
              </section>
            </div>
          </div>
      }
    </div>
  )
}

export default Home;

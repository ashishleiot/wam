import { useState } from 'react';

import axios from 'axios';
import { Redirect } from 'react-router-dom';

const WAMessage = (props) => {

  const [message, setMessage] = useState('');
  const [isBtnDisabled, setBtnState] = useState(false);

  const sendMessage = (event)  => {
    //disable button
    if (isBtnDisabled) {
      alert('Kindly wait, message is being sent');
    } else {
      setBtnState(true);
      const data = {
        'message': `${message}`
      }
      axios.post('https://wam-backend2.herokuapp.com/sendMessage', data).then((status) => {
        console.log(status);
        setBtnState(false);
      }).catch((err) => {
        console.log(err);
        setBtnState(false);
      });
    }
    event.preventDefault();
  }

  return (
    <div>
      {
        props.user
          ?
        <form onSubmit={sendMessage}>
          <textarea onChange={(event) => setMessage(event.target.value)} placeholder="Enter message..." value={message} />
          <input type="submit" value="Send Message" />
        </form>
          :
        <Redirect to="/" />
      }
    </div>
  )
}

export default WAMessage;

import {Redirect} from 'react-router-dom';



const Dashboard = (props) => {
  console.log(props);
  return(
    <div>
      {
        props.user 
          ?
        <div>
          <p>This is Dashboard</p>
        </div>
          :
        <Redirect to="/" />
      }
    </div>
  )
}

export default Dashboard;

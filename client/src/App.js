import './App.css';
import React from 'react';
import { useState, useEffect } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import firebase from 'firebase/app';
import 'firebase/auth';

import Login from './views/Login';
import Home from './views/Home';
import Dashboard from './views/Dashboard';
import Message from './views/Message';
import Navbar from './components/Navbar';

import axios from 'axios';
import dotenv from 'dotenv';



dotenv.config();

firebase.initializeApp({
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId:  process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT_ID
});

function App() {

  const [user, setUser] = useState(null);

  function onSetUser(user) {
    setUser(user);
  }

  const auth = firebase.auth();

  const uiConfig = {
    signInFlow: 'popup',
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    ], 
    callbacks: {
      signInSuccessWithAuthResult: (userObj) => {
        if(userObj.additionalUserInfo.isNewUser) {
          const data = {
            'uid': `${userObj.user.uid}`,
            'name': `${userObj.user.displayName}`,
            'email': `${userObj.user.email}`
          }
          axios.post('https://wam-backend2.herokuapp.com/registerUser', data).then(() => {
            console.log('user registered');
          }).catch(() => {
            console.log('error registering user');
          })
        }
      }
    }
  }

  useEffect(() => {
    auth.onAuthStateChanged(user => {
      setUser(user);
      if (user) {
        <Redirect to="/dashboard" />
      }
    })
  }, [auth]);

  const logOut = () => {
    auth.signOut().then(() => {
      console.log('logged out');
    }).catch((err) => {
      console.log(err.message);
    })
  }

  return (

    <div>
      {user ? <Navbar onLogout={logOut}/> : <div></div>}
      <Switch>
        <Route exact path="/">
          <Home
            user={user}
            uiConfig={uiConfig}
            auth={auth}
            onSetUser={onSetUser}
          />
        </Route>
        <Route path="/login">
          <Login
            user={user}
            uiConfig={uiConfig}
            auth={auth}
            onSetUser={onSetUser}
          />
        </Route>
        <Route path="/dashboard">
          <Dashboard 
            user={user}
            onLogout={logOut}
          />
        </Route>
        <Route path="/message">
          <Message 
            user={user}
          />
        </Route>
      </Switch>
    </div>
  );
}

export default App;

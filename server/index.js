const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const dotenv = require('dotenv');
const mongo = require('mongodb');

const app = express();

app.use(bodyParser.urlencoded({extended: true, limit: '1mb'}));
app.use(bodyParser.json());
app.use(cors());

dotenv.config();

const PORT = 5000;

app.get('/', (req, res) => {
  res.send('Hello from Express!');
})

app.post('/registerUser', async (req, res) => {
  const uri = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0-lj2nn.mongodb.net/${process.env.MONGO_DBNAME}?retryWrites=true&w=majority`;
  const client = new mongo.MongoClient(uri, {useUnifiedTopology: true});

  try {
    await client.connect();
    const data = req.body;
    await client.db("wam").collection(`wam_users`).insertOne(data);
    res.status(200).send({
      result: 'success',
    })
  } catch (err) {
    res.status(400).send({
      result: 'error',
      message: err
    })
  } finally {
    await client.close();
  }

});

app.post('/sendMessage', (req, res) => {
  const message = req.body.message;
  const accountSid = process.env.TWILIO_ACCOUNT_SID;
  const authToken = process.env.TWILIO_AUTH_TOKEN;
  const client = require('twilio')(accountSid, authToken);

  client.messages
    .create({
      from: 'whatsapp:+14155238886',
      body: message,
      to: `${process.env.PHONE_NO}`,
    }).then(message => {
        res.status(200).send({
          result: 'success',
        })
      }).catch((err) => {
        console.log(err);
        res.status(400).send({
          result: 'error',
          error: err,
        })
      })
      .catch((err) => {
        console.log(err);
        res.send(err);
      });

})

app.listen(process.env.PORT || PORT, () => {
  console.log(`listening to ${process.env.PORT || PORT}`);
})
